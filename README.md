AbcFileDistributionBundle
=============

The AbcFileDistributionBundle provides a database-backed file management system for Symfony.
It gives you a flexible framework for storing and transferring files between various locations (Local, FTP, CDN).

Features include:

- Filesystem definitions can be stored via Doctrine ORM, MongoDB/CouchDB ODM or Propel
- Filesystem definitions can be defined in configuration 
- Unit tested


Documentation
-------------

The source of the documentation is stored in the `Resources/doc/` folder in this bundle.


Installation
------------

All the installation instructions are located in the documentation.

License
-------

This bundle is under the MIT license. See the complete license in the bundle:

    Resources/meta/LICENSE

About
-----

AbcFileDistributionBundle is a [AboutCoders](https://aboutcoders.com) initiative.

Reporting an issue or a feature request
---------------------------------------

Issues and feature requests are tracked in the [BitBucket issue tracker](https://bitbucket.org/wciolko/file-distribution-bundle/issues).

When reporting a bug, it may be a good idea to reproduce it in a basic project
built using the [Symfony Standard Edition](https://github.com/symfony/symfony-standard)
to allow developers of the bundle to reproduce the issue by simply cloning it
and following some steps.

